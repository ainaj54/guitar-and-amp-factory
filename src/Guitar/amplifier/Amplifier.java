package Guitar.amplifier;

import Guitar.Guitar;
import Guitar.InvalidGuitarException;
import Guitar.Playable;

public abstract class Amplifier implements Playable {
    private String brand;
    private String model;
    protected int ampVolume;
    private int wattage;
    protected Boolean ampTurnedOn;
    protected Guitar connectedGuitar;


    public Amplifier(String brand, String model, int wattage, Boolean ampTurnedOn) {
        this.brand = brand;
        this.model = model;
        this.wattage = wattage;
        this.ampTurnedOn = ampTurnedOn;
    }

    public void connectGuitar(Guitar inputGuitar) throws InvalidGuitarException {
        if (connectedGuitar != null) {
            System.out.println("Disconnect connected guitar first");
            return;
        }
        if (inputGuitar.getHasPickUp()) {
            connectedGuitar = inputGuitar;
            System.out.println(connectedGuitar.getBrand() + " has been connected");
        } else {
            throw new InvalidGuitarException("Invalid Guitar");
        }
    }
    public void disconnectGuitar(){
        if(connectedGuitar != null) {
            System.out.println( connectedGuitar.getBrand() + " has been disconnected.");
            connectedGuitar = null;

        }else{
            System.out.println("No guitar to disconnect");
        }
    }
    public void turnVolumUp(){
        ampVolume += 10;
    }


    public void makeSound() {
        if (!ampTurnedOn){
            System.out.println("Amplifier has not been turned on. ");
        }
        if (ampTurnedOn){
            if(connectedGuitar != null){
                connectedGuitar.makeSound();
                System.out.println("Volume is " + ampVolume + " dB.");
            }else {
                System.out.println("STATIC NOISE!!!");
            }
        }

    }

}
