package Guitar.amplifier;

public interface ChannelManager {
    void raiseChannelVolume();
    void lowerChannelVolume();
}
