package Guitar.amplifier;

import java.util.HashMap;

public class FourChannelAmplifier extends Amplifier implements MultiChannelManager {
    private HashMap<Integer,Channel> ampChannels = new HashMap<>();
    private Channel currentChannel;


    public FourChannelAmplifier(String brand, String model, int wattage, Boolean ampTurnedOn,Channel channelOne, Channel channelTwo, Channel channelThree, Channel channelFour) {
        super(brand, model, wattage, ampTurnedOn);
        ampChannels.put(1, channelOne);
        ampChannels.put(2, channelTwo);
        ampChannels.put(3, channelThree);
        ampChannels.put(4, channelFour);
        currentChannel = channelOne;

    }

    @Override
    public void raiseChannelVolume() {
        currentChannel.raiseChannelVolume();
        this.ampVolume = currentChannel.getChannelVolume();
    }

    @Override
    public void lowerChannelVolume() {
        currentChannel.lowerChannelVolume();
        this.ampVolume = currentChannel.getChannelVolume();
    }

    @Override
    public void switchChannel(int wantedChannel) {
        if(ampChannels.get(wantedChannel) == null){
            System.out.println("Not valid channel");
        }else{
            currentChannel = ampChannels.get(wantedChannel);
        }
    }


    @Override
    public void makeSound() {
        if (!ampTurnedOn){
            System.out.println("Amplifier has not been turned on. ");
        }
        if (ampTurnedOn){
            if(connectedGuitar != null){
                System.out.println(currentChannel.getSound());
                connectedGuitar.makeSound();
                System.out.println(currentChannel.getSound());

            }else {
                System.out.println("STATIC NOISE!!!");
            }
        }

    }

}
