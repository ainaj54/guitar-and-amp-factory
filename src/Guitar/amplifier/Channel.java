package Guitar.amplifier;
//Channel Object
public class Channel implements ChannelManager {
    private ChannelSounds sound;
    private int channelVolume;

    public Channel(ChannelSounds sound) {
        this.sound = sound;
    }

    @Override
    public void raiseChannelVolume() {
        if (channelVolume > 100){

        }else{
            channelVolume += 10;

        }
    }

    @Override
    public void lowerChannelVolume() {
        if (channelVolume <= 10){
            channelVolume = 0;

        }else{
            channelVolume -= 10;

        }
    }

    public void setSound(ChannelSounds sound) {
        this.sound = sound;
    }

    public ChannelSounds getSound() {
        return sound;
    }

    public int getChannelVolume() {
        return channelVolume;
    }

}
