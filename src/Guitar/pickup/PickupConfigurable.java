package Guitar.pickup;

public interface PickupConfigurable {
    String getPickup();
}
