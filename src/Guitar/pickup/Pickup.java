package Guitar.pickup;

public class Pickup {
    private String name;


    public Pickup(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
