package Guitar.acousticGuitars;

import Guitar.Guitar;

public abstract class AcousticGuitarInstruments extends Guitar {


    public AcousticGuitarInstruments(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);
        hasPickUp = false;
    }

    @Override
    public void makeSound() {
        System.out.println("generic drrriiiinnng");
    }
}

