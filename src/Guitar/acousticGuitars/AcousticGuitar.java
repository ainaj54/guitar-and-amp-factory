package Guitar.acousticGuitars;

import Guitar.Guitar;

public class AcousticGuitar extends AcousticGuitarInstruments{


    public AcousticGuitar(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);
    }

    @Override
    public void makeSound() {
        System.out.println("drrriiiinnng");
    }
}

