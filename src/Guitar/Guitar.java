package Guitar;

public abstract class Guitar implements Playable{
    private String brand;
    private String model;
    private int manufacturedYear;
    protected Boolean hasPickUp;
    private String guitarSound;

    public Guitar(){

    }
    public Guitar(String brand, String model, int manufacturedYear) {
        this.brand = brand;
        this.model = model;
        this.manufacturedYear = manufacturedYear;
        this.guitarSound = "Generic guitar sound";
    }

    @Override
    public void makeSound() {
        System.out.println("Generic guitar sound.");;
    }

    public String getBrand() {
        return brand;
    }

    public Boolean getHasPickUp(){
        return this.hasPickUp;
    }
}
