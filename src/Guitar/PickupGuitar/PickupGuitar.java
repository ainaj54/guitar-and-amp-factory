package Guitar.PickupGuitar;

import Guitar.Guitar;
import Guitar.pickup.Pickup;
import Guitar.pickup.PickupConfigurable;

public abstract class PickupGuitar extends Guitar implements PickupConfigurable {
   private Pickup guitarPickup;


    public PickupGuitar(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);
        this. hasPickUp = true;
        this.guitarPickup = new Pickup("brandName");
    }

    @Override
    public String getPickup() {

        return guitarPickup.getName();
    }

    @Override
    public void makeSound() {
        System.out.println("Generic TWANG!");
    }
}
