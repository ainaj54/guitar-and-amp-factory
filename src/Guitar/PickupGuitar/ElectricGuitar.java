package Guitar.PickupGuitar;

import Guitar.Guitar;

public class ElectricGuitar extends PickupGuitar {

    public ElectricGuitar(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);


    }

    @Override
    public void makeSound() {
        System.out.println("Twaaaaang");
    }


}
