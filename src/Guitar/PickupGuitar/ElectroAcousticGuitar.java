package Guitar.PickupGuitar;

import Guitar.Guitar;

public class ElectroAcousticGuitar extends PickupGuitar {


    public ElectroAcousticGuitar(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);


    }

    @Override
    public void makeSound() {
        System.out.println("DRIIINGTWAAANG");
    }
}
