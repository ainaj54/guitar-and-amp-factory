package scania.aina;


import Guitar.*;
import Guitar.PickupGuitar.ElectricGuitar;
import Guitar.PickupGuitar.ElectroAcousticGuitar;
import Guitar.PickupGuitar.PickupGuitar;
import Guitar.amplifier.Channel;
import Guitar.amplifier.ChannelSounds;
import Guitar.amplifier.FourChannelAmplifier;
import Guitar.amplifier.SingleChannelAmplifier;


import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        PickupGuitar myGuitar = new ElectricGuitar("Electric brand", "model", 1992);
        PickupGuitar acousticGuitar = new ElectroAcousticGuitar("Electroacoustic brand","nicemodel", 1962);

        ArrayList<Guitar> myGuitarCollection = new ArrayList<Guitar>();
        myGuitarCollection.add(myGuitar);
        myGuitarCollection.add(acousticGuitar);

        SingleChannelAmplifier myAmp = new SingleChannelAmplifier("brand", "model", 1000, true);

        try {
            myAmp.connectGuitar(acousticGuitar);
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            myAmp.connectGuitar(myGuitar);
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        myAmp.disconnectGuitar();

        for(Guitar guitar: myGuitarCollection){
            guitar.makeSound();
        }

        myAmp.makeSound();

        try {
            myAmp.connectGuitar(acousticGuitar);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        myAmp.makeSound();
        myAmp.turnVolumUp();
        myAmp.makeSound();
        System.out.println(myGuitar.getPickup());


        Channel channelOne = new Channel(ChannelSounds.DRAAAMMMM);
        Channel channelTwo = new Channel(ChannelSounds.PLING);
        Channel channelThree = new Channel(ChannelSounds.VROOOM);
        Channel channelFour = new Channel(ChannelSounds.SWOOOSHH);

        System.out.println("Trying fourchannelAmp");
        FourChannelAmplifier myFourChannelAmp = new FourChannelAmplifier("brand", "model", 100, true, channelOne, channelTwo, channelThree, channelFour);
        try {
            myFourChannelAmp.connectGuitar(acousticGuitar);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        myFourChannelAmp.makeSound();

        myFourChannelAmp.switchChannel(3);

        myFourChannelAmp.makeSound();


     }
}
